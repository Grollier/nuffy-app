# Nuffy application

## Application de blagues

API : https://www.blablagues.net/apix.html
Il n'y a pas besoin de token pour y accéder.
Le swagger est disponible à l'adresse précédente.

### Fonctionnalités de l'application

Nuffy est une application regroupant plusieurs blagues provenant de l'API de **Blablagues** (voir lien plus haut).

*Ecran home* :
On a une liste de catégories dans un select. Lorsqu'on sélectionne une catégorie, 5 blagues aléatoires sont affichées sur
l'écran. On peut ensuite choisir de cliquer sur une blagues pour afficher les détails et par conséquent la blague complète. 
On peut aussi choisir de n'afficher que les blagues écrites par l'utilisateur.

*Ecran blagues détails* :
On retrouve le contenu de la blague avec différents éléments, comme les likes / dislikes par exemple. On peut retourner ensuite à la page d'accueil.

*Ecran d'ajout d'une blague*
En cliquant sur le (+) dans l'AppBar, on peut accéder à l'écran d'ajout d'une blague. On doit rentrer différents critères pour
pouvoir ajouter une blague dont la catégorie, le texte et le nom d'auteur. Après avoir validé le formulaire, la blague est disponible
pour être lue sur l'écran Home.

Bien sûr, de nombreuses fonctionnalités tendent à être améliorée, comme l'ajout des blagues qui est uniquement en SharedPreferences pour l'instant,
ajouter un système d'inscription / connexion pour justement gérer ces blagues. Cela reste une première version relativement simple
en termes de fonctionnalités mais qui permet déjà de retrouver l'objectif principal, rigoler à des blagues catégorisées et aléatoires. (Il se peut que certaines blagues reviennent plusieurs fois, l'API n'a pas un stock illimité :)

### Version

Flutter 2.10.0
Dart 2.16.0

Copyright [2022] [Théo Grollier] All Right Reserved.




