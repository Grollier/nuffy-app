import 'dart:convert';

import 'package:http/http.dart';

class JokesRepository {

  /// Get random jokes from chosen category.
  Future<List<dynamic>> fetchJokesByCategory(String category) async {
    final Response response = await get(Uri.parse('https://api.blablagues.net/?rub=blagues&cat=$category&nb=5'));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load jokes of category : $category');
    }
  }
  
  /// Get a random jokes from random category.
  Future<Map<String, dynamic>> fetchRandomJoke() async {
    final Response response = await get(
        Uri.parse('https://api.blablagues.net/?rub=blagues'));
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load random joke.');
    }
  }

  Future<Map<String, dynamic>> getJokesCategories() async {
    final Response response = await get(
      Uri.parse('https://api.blablagues.net/?list_cat')
    );
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      throw Exception('Failed to load categories.');
    }
  }
}