import 'dart:convert';

import 'package:nuffy_app/models/category.dart';
import 'package:nuffy_app/repository/jokes_repository.dart';
import 'package:nuffy_app/models/joke.dart';
import 'package:nuffy_app/repository/preferences_repository.dart';

class Repository {
  JokesRepository jokesRepository;
  PreferencesRepository preferencesRepository;

  Repository(this.jokesRepository, this.preferencesRepository);

  /// Consult joke by category name.
  Future<List<Joke>> consultJokesByCategory(String category) async {
    if (category.contains(' ')) {
      category.replaceAll(' ', '+');
    }
    List<Joke> jokes = [];
    List<dynamic> jsonJokes =
        await jokesRepository.fetchJokesByCategory(category);
    for (var joke in jsonJokes) {
      jokes.add(Joke.fromJson(joke));
    }
    return jokes;
  }

  /// Get all categories.
  Future<List<Category>> getAllCategories() async {
    final List<Category> categories = [];
    final Map<String, dynamic> response = await jokesRepository.getJokesCategories();
    final Map<String, dynamic> blaguesCategory = response['blagues'];

    List<dynamic> categoriesId = [];
    for (dynamic categoryId in blaguesCategory.keys) {
      categoriesId.add(categoryId);
    }

    int i = 0;
    for (dynamic category in blaguesCategory.values) {
      categories.add(Category.fromJson(category, categoriesId[i]));
      i++;
    }
    return categories;
  }

  /// Save a custom joke in shared preferences.
  Future<void> saveCustomJoke(List<Joke> jokes) async {
    final List<String> jsonJokes = jokes.map((joke) => jsonEncode(joke.toJson())).toList();
    await preferencesRepository.saveJoke(jsonJokes);
  }

  /// Load jokes from shared preferences.
  Future<List<Joke>> loadCustomJokes() async {
    final List<String> jsonJokes = await preferencesRepository.loadJokes();
    return jsonJokes.map((json) => Joke.fromJson(jsonDecode(json))).toList();
  }
}
