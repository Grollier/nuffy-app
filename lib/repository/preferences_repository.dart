import 'package:shared_preferences/shared_preferences.dart';

class PreferencesRepository {

  static const String jokesKey = 'jokes';

  Future<void> saveJoke(List<String> jokes) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList(jokesKey, jokes);
  }

  Future<List<String>> loadJokes() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(jokesKey) ?? [];
  }
}