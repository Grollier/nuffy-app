import 'package:flutter_bloc/flutter_bloc.dart';

import '../models/joke.dart';
import '../repository/repository.dart';

class CustomJokeCubit extends Cubit<List<Joke>> {

  CustomJokeCubit(this._repository) : super([]);
  final Repository _repository;

  void addCustomJoke(Joke joke) {
    emit([...state, joke]);
    _repository.saveCustomJoke(state);
  }

  Future<void> loadCustomJokes() async {
    final List<Joke> jokes = await _repository.loadCustomJokes();
    emit(jokes);
  }

}