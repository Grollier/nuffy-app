import 'package:flutter_bloc/flutter_bloc.dart';

import '../models/category.dart';
import '../repository/repository.dart';

class CategoryCubit extends Cubit<List<Category>> {

  CategoryCubit(this._repository) : super([]);
  final Repository _repository;

  Future<void> loadCategories() async {
    final List<Category> categories = await _repository.getAllCategories();
    emit(categories);
  }

}