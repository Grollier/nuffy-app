/// Class helpful to manipulating categories inside the application.
/// This isn't the real object from our API. It's a custom object.
class Category {

  final String categoryId;

  final String category;

  final String titre;

  final String total;

  Category(this.categoryId, this.category, this.titre, this.total);

  /// Mapper to create a category.
  factory Category.fromJson(Map<String, dynamic> json, String categoryId) {
    String category = json['categorie'];
    String titre = json['titre'];
    String total = json['total'];

    return Category(categoryId, category, titre, total);
  }

  @override
  String toString() {
    return 'Category{category: $category, titre: $titre, total: $total}';
  }
}