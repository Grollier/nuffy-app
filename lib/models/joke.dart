class Joke {

  final String rubrique;

  final String categorie;

  final String date;

  final String like;

  final String unlike;

  /// Teaser text for the joke.
  final String textHead;

  /// Story of the joke if it's not a riddle.
  final String textJoke;

  /// Answer of the riddle.
  final String textHidden;

  final String pseudoAuthor;

  Joke(this.rubrique, this.categorie, this.date, this.like, this.unlike,
      this.textHead, this.textJoke, this.textHidden, this.pseudoAuthor);

  /// Object to Json.
  Map<String, dynamic> toJson() {
    return {
      'rubrique': rubrique,
      'categorie': categorie,
      'date': date,
      'like': like,
      'dislike': unlike,
      'textHead': textHead,
      'textJoke': textJoke,
      'textHidden': textHidden,
      'author': pseudoAuthor,
    };
  }


  /// Mapper to create a joke.
  factory Joke.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> data = json['data'] ?? {};
    String rubrique = data['rubrique'];
    String categorie = data['categorie'];
    String date = data['date'];
    String like = data['like'];
    String unlike = data['unlike'];

    Map<String, dynamic> content = data['content'] ?? {};
    String textHead = content['text_head'];
    String textJoke = content['text'];
    String textHidden = content['text_hidden'];

    Map<String, dynamic> author = data['author'] ?? {};
    String pseudoAuthor = author['pseudo'];

    return Joke(rubrique, categorie, date, like, unlike, textHead, textJoke, textHidden, pseudoAuthor);
  }

  @override
  String toString() {
    return 'Joke{rubrique: $rubrique, categorie: $categorie, date: $date, like: $like, unlike: $unlike, textHead: $textHead, textJoke: $textJoke, textHidden: $textHidden, pseudoAuthor: $pseudoAuthor}';
  }
}