import 'package:flutter/material.dart';
import 'package:nuffy_app/blocs/custom_joke_cubit.dart';
import 'package:nuffy_app/repository/jokes_repository.dart';
import 'package:nuffy_app/repository/preferences_repository.dart';
import 'package:nuffy_app/repository/repository.dart';
import 'package:nuffy_app/ui/screen/add_joke.dart';
import 'package:nuffy_app/ui/screen/home.dart';
import 'package:nuffy_app/ui/screen/joke_details.dart';
import 'package:provider/provider.dart';

void main() async {
  final PreferencesRepository preferencesRepository = PreferencesRepository();
  final JokesRepository jokesRepository = JokesRepository();
  final Repository repository = Repository(jokesRepository, preferencesRepository);
  final CustomJokeCubit customJokeCubit = CustomJokeCubit(repository);
  await customJokeCubit.loadCustomJokes();
  runApp(
    MultiProvider(
      providers: [
        Provider<CustomJokeCubit>(create: (_) => customJokeCubit),
        Provider<Repository>(create: (_) => repository),
      ],
      child: const MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Nuffy',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        fontFamily: 'Cafe-Matcha',
      ),
      routes: {
        '/home': (context) => const Home(),
        '/addJoke': (context) => AddJoke(),
        '/jokeDetails': (context) => JokeDetails()
      },
      initialRoute: '/home',
    );
  }
}
