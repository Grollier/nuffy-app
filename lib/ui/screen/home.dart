import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nuffy_app/blocs/custom_joke_cubit.dart';
import 'package:nuffy_app/models/category.dart';
import 'package:nuffy_app/models/joke.dart';
import 'package:nuffy_app/repository/jokes_repository.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:nuffy_app/repository/preferences_repository.dart';
import 'package:nuffy_app/repository/repository.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final Repository _repository =
      Repository(JokesRepository(), PreferencesRepository());
  List<Joke> _jokes = [];
  final Set<Category> _seen = {};
  List<Category> _categories = [];
  String? _selectedValue;
  bool _isChecked = false;

  @override
  void initState() {
    _repository.getAllCategories().then((res) {
      _categories = res.where((category) => _seen.add(category)).toList();
      if (_categories.isNotEmpty) {
        _selectedValue = _categories[0].category;
      }
      setState(() {

      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.green;
      }
      return Colors.red;
    }

    return Scaffold(
        appBar: AppBar(
          elevation: 10,
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                IconButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed('/addJoke');
                  },
                  icon: const Icon(Icons.add, color: Colors.white),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.account_circle, color: Colors.white),
                ),
              ],
            ),
          ],
          title: BorderedText(
            strokeWidth: 2.0,
            strokeColor: Colors.deepOrange,
            child: const Text('Nuffy',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    decorationStyle: TextDecorationStyle.wavy)),
          ),
          leading: IconButton(
            onPressed: () {},
            icon: const Icon(Icons.home, color: Colors.white),
          ),
        ),
        body: Container(
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            stops: [0.2, 1],
            colors: [Colors.orangeAccent, Colors.yellow],
          )),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  BorderedText(
                    strokeWidth: 2.0,
                    strokeColor: Colors.deepOrange,
                    child: const Text(
                      'Choisissez une catégorie de blagues ->',
                      style: TextStyle(
                        fontSize: 30,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(width: 20),
                  DropdownButton<String>(
                    value: _selectedValue,
                    onChanged: (value) {
                      _selectedValue = value!;
                      for (Category category in _categories) {
                        if (category.category == _selectedValue) {
                          _repository
                              .consultJokesByCategory(category.categoryId)
                              .then((result) {
                            setState(() {
                              _jokes = result;
                              _isChecked = false;
                            });
                          });
                        }
                      }
                    },
                    items: _categories.map((itemOne) {
                      return DropdownMenuItem<String>(
                        value: itemOne.category,
                        child: Text(
                          itemOne.category,
                          style: const TextStyle(fontSize: 25),
                        ),
                      );
                    }).toList(),
                  ),
                  const SizedBox(width: 20),
                  BlocBuilder<CustomJokeCubit, List<Joke>>(
                      builder: (context, _customJokes) {
                    return Checkbox(
                      checkColor: Colors.white,
                      fillColor: MaterialStateProperty.resolveWith(getColor),
                      value: _isChecked,
                      onChanged: (bool? value) {
                        setState(() {
                          _isChecked = value!;
                          if (_isChecked) {
                            _customJokes.shuffle();
                            List<Joke> listCustomJokes = [];
                            int i = 0;
                            for (Joke joke in _customJokes) {
                              if (_customJokes.length > 5) {
                                while (i <= 5) {
                                  listCustomJokes.add(joke);
                                  i++;
                                }
                              } else {
                                listCustomJokes.add(joke);
                              }
                            }
                            _jokes = listCustomJokes;
                          }
                          else {
                            _jokes = [];
                          }
                        });
                      },
                    );
                  }),
                  const SizedBox(width: 10),
                  const Text(
                    'Afficher uniquement les blagues personnelles',
                    style: TextStyle(fontSize: 15),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(
                      onPressed: () {
                        for (Category category in _categories) {
                          if (category.category == _selectedValue) {
                            _repository
                                .consultJokesByCategory(category.categoryId)
                                .then((result) {
                              setState(() {
                                _jokes = result;
                                _isChecked = false;
                              });
                            });
                          }
                        }
                      },
                      icon: const Icon(
                        Icons.refresh,
                        color: Colors.white,
                        size: 30,
                      )),
                ],
              ),
              Expanded(
                child: ListView.separated(
                    itemBuilder: (BuildContext context, int index) {
                      final Joke joke = _jokes[index];
                      return Card(
                        elevation: 5,
                        margin: const EdgeInsets.all(10),
                        color: Colors.white70,
                        child: ListTile(
                          leading: const Icon(Icons.arrow_forward_ios),
                          onTap: () {
                            Navigator.of(context).pushNamed('/jokeDetails',
                                arguments: {'joke': joke});
                          },
                          title: BorderedText(
                            strokeWidth: 1.0,
                            strokeColor: Colors.orangeAccent,
                            child: Text(
                              joke.textHead,
                              style: const TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 35,
                              ),
                            ),
                          ),
                          subtitle: BorderedText(
                            strokeWidth: 1.0,
                            strokeColor: Colors.orangeAccent,
                            child: Text(
                              'par ' + joke.pseudoAuthor + " " + 'publié le : ' + joke.date,
                              style: const TextStyle(fontSize: 20),
                            ),
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) => const Divider(
                          height: 0,
                        ),
                    itemCount: _jokes.length),
              ),
            ],
          ),
        ));
  }
}
