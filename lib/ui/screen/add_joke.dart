import 'package:flutter/material.dart';
import 'package:nuffy_app/blocs/custom_joke_cubit.dart';
import 'package:nuffy_app/models/joke.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:provider/provider.dart';
import 'package:nuffy_app/repository/preferences_repository.dart';
import '../../models/category.dart';
import '../../repository/jokes_repository.dart';
import '../../repository/repository.dart';

class AddJoke extends StatefulWidget {
  AddJoke({Key? key}) : super(key: key);

  @override
  State<AddJoke> createState() => _AddJokeState();
}

class _AddJokeState extends State<AddJoke> {
  final Repository _repository =
      Repository(JokesRepository(), PreferencesRepository());
  final GlobalKey<FormState> _formKey = GlobalKey();

  final TextEditingController _textAccrocheEditingController =
      TextEditingController();
  final TextEditingController _textJokeEditingController =
      TextEditingController();
  final TextEditingController _textAuthorNameEditingController =
      TextEditingController();

  String? _selectedValue;

  final Set<Category> _seen = {};

  List<Category> categories = [];

  @override
  void initState() {
    _repository.getAllCategories().then((res) {
      categories = res.where((category) => _seen.add(category)).toList();
      if (categories.isNotEmpty) {
        _selectedValue = categories[0].category;
      }
      setState(() {

      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 10,
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.account_circle, color: Colors.white),
                ),
              ],
            ),
          ],
          title: BorderedText(
            strokeWidth: 2.0,
            strokeColor: Colors.deepOrange,
            child: const Text('Nuffy',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    decorationStyle: TextDecorationStyle.wavy)),
          ),
          leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushNamed('/home');
            },
            icon: const Icon(Icons.home, color: Colors.white),
          ),
        ),
        body: Container(
            margin: const EdgeInsets.all(8),
            child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      BorderedText(
                        strokeWidth: 1.0,
                        strokeColor: Colors.orangeAccent,
                        child: const Text(
                          'Ecrivez votre propre blague !',
                          style: TextStyle(fontSize: 40),
                        ),
                      ),
                    ]),
                    const SizedBox(height: 20),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      const Text('Choisissez une catégorie : ',
                          style: TextStyle(fontSize: 25)),
                      const SizedBox(width: 20),
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: DropdownButton<String>(
                            value: _selectedValue,
                            onChanged: (value) {
                              setState(() {
                                _selectedValue = value!;
                              });
                            },
                            items: categories.map((itemOne) {
                              return DropdownMenuItem<String>(
                                value: itemOne.category,
                                child: Text(
                                  itemOne.category,
                                  style: const TextStyle(fontSize: 25),
                                ),
                              );
                            }).toList(),
                          )),
                    ]),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: TextFormField(
                        controller: _textAccrocheEditingController,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Le début de la blague doit être renseigné';
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                            prefixIcon:
                                const Icon(Icons.document_scanner_rounded),
                            labelText:
                                "Début / accroche, peut aussi être le titre de la blague (exemple : C'est deux blondes qui discutent)",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10))),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 10,
                        controller: _textJokeEditingController,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'La blague doit être renseignée sinon ce n\'est pas drôle !';
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                            prefixIcon:
                                const Icon(Icons.document_scanner_rounded),
                            labelText:
                                "Votre blague (marrante sinon ce n'est pas une blague)",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10))),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: TextFormField(
                        controller: _textAuthorNameEditingController,
                        validator: (String? value) {
                          if (value == null || value.isEmpty) {
                            return 'Votre nom d\'auteur est nécessaire ! On veut savoir qui est le clown';
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                            prefixIcon:
                                const Icon(Icons.document_scanner_rounded),
                            labelText:
                                "Votre petit nom d'auteur de cette blague incroyable",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10))),
                      ),
                    ),
                    const SizedBox(height: 30),
                    ElevatedButton(
                        onPressed: () {
                          validate(context);
                        },
                        child: const Text(
                          'Envoyer la blague',
                          style: TextStyle(fontSize: 20),
                        ))
                  ],
                ))));
  }

  void validate(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      final String textHead = _textAccrocheEditingController.text;
      final String textJoke = _textJokeEditingController.text;
      final String textAuthor = _textAuthorNameEditingController.text;
      final Joke joke = Joke(
          "blagues",
          _selectedValue!,
          DateTime.now().toString(),
          "0",
          "0",
          textHead,
          textJoke,
          "",
          textAuthor);
      Provider.of<CustomJokeCubit>(context, listen: false).addCustomJoke(joke);
      Navigator.of(context).pushNamed('/home');
    }
  }
}
