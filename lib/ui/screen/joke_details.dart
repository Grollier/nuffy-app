import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nuffy_app/models/joke.dart';
import 'package:bordered_text/bordered_text.dart';

class JokeDetails extends StatelessWidget {
  const JokeDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> args =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    final Joke joke = args['joke'];

    return Scaffold(
      appBar: AppBar(
        elevation: 10,
        actions: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                onPressed: () {},
                icon: const Icon(Icons.add, color: Colors.white),
              ),
              IconButton(
                onPressed: () {},
                icon: const Icon(Icons.account_circle, color: Colors.white),
              ),
            ],
          ),
        ],
        title: BorderedText(
          strokeWidth: 2.0,
          strokeColor: Colors.deepOrange,
          child: const Text('Nuffy',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                  decorationStyle: TextDecorationStyle.wavy)),
        ),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pushNamed('/home');
          },
          icon: const Icon(Icons.home, color: Colors.white),
        ),
      ),
      body: Container(
        margin: const EdgeInsets.all(8),
        child: Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            BorderedText(
              strokeWidth: 2.0,
              strokeColor: Colors.deepOrange,
              child: const Text(
                'Détails de la blague',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 35),
              ),
            ),
          ]),
          const SizedBox(height: 20),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            BorderedText(
              strokeWidth: 2.0,
              strokeColor: Colors.orange,
              child: Text(
                joke.textHead,
                style: const TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
          ]),
          const SizedBox(height: 20),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            BorderedText(
              strokeWidth: 2.0,
              strokeColor: Colors.orange,
              child: Text(
                joke.textJoke,
                style: const TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            BorderedText(
              strokeWidth: 2.0,
              strokeColor: Colors.orange,
              child: Text(
                joke.textHidden,
                style: const TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
          ]),
          const SizedBox(height: 20),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                const Icon(
                  Icons.thumb_up,
                  color: Colors.green,
                ),
                BorderedText(
                  strokeWidth: 2.0,
                  strokeColor: Colors.green,
                  child: Text(
                    joke.like,
                    style: const TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
                const Icon(
                  Icons.thumb_down,
                  color: Colors.red,
                ),
                BorderedText(
                  strokeWidth: 2.0,
                  strokeColor: Colors.red,
                  child: Text(
                    joke.unlike,
                    style: const TextStyle(color: Colors.white, fontSize: 25),
                  ),
                ),
              ]),
          const SizedBox(height: 20),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            BorderedText(
              strokeWidth: 2.0,
              strokeColor: Colors.black,
              child: Text(
                'Le grand blagueur auteur de cette blague est : ' +
                    joke.pseudoAuthor,
                style: const TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
          ]),
          const SizedBox(height: 20),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/home');
                },
                child: const Text(
                  'Retourner voir d\'autres blagues',
                  style: TextStyle(fontSize: 20),
                ))
          ]),
        ]),
      ),
    );
  }
}
